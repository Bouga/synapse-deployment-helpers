# Synapse deployment helpers

This project aims to help publishing and deploying packages and projects during weekly deployment. It's highly iterative and is bound to evole with use.

This will start in the form of a [Oh-my-zsh](https://ohmyz.sh/) plugin, and bash support may be added later. In general, all shell can be implemented with some installation process (functions syntax might have to change depending on shell or platform).

- [Current support](#current-support)
- [Installation](#installation)
  - [Oh-my-zsh plugin](#oh-my-zsh-plugin)
  - [Bash](#bash)
  - [Fish](#fish)
- [Usage](#usage)
  - [Deployment helpers](#deployment-helpers)
  - [Git utilities](#git-utilities)
  - [Utils](#utils)
- [Support](#support)
- [Roadmap](#roadmap)
- [Project status](#project-status)

---
## Current support

![](https://img.shields.io/badge/platform-osx-lightgrey) ![](https://img.shields.io/badge/shell-oh--my--zsh-blue) ![](https://img.shields.io/badge/shell-bash-orange)

## Installation

### Oh-my-zsh plugin

> Requirement : [Oh-my-zsh](https://ohmyz.sh/)

Run this to copy the plugin in the custom omz plugin location (`$ZSH_CUTSOM/plugins`). Zsh shell will be sourced with `exec zsh`.

```sh
git clone https://gitlab.com/Bouga/synapse-deployment-helpers.git
cd synapse-deployment-helpers
make zsh
```

### Bash

```sh
git clone https://gitlab.com/Bouga/synapse-deployment-helpers.git
cd synapse-deployment-helpers
make bash

# Follow instructions, input your bash init file location and name (defaults to $HOME/.bashrc)
# If $HOME is not defined at this point, script will exit

# "Where is your bash init file ? (defaults to $HOME): "
# "What is your bash init file ? (defaults to .bashrc): "

# A backup of your bash init file will be created just in case (defaults to ~/.bashrc.pre-synapse)

cd $HOME
source .bashrc # or your bash init file name
```

### Fish ?

> #TODO fish installation

---
## Usage

### Deployment helpers

Commands directly aimed to facilitate deployment of apps and packages.

#### `cimsg`

```sh
# Copies a chatops message in the clipboard (is meant to be posted to #log-chatops slack channel to start a ci build)

cd ~/synapse-deployment-helpers
git checkout main
cimsg 1.22.14-rc4
```

Outputs:

```
Copied to clipboard:
/gitlab ci run tag synapse-deployment-helpers main 1.22.14-rc4
```

### Git utilities

Git related commands.

#### `gccb`

```sh
# Copies the current branche name in the clipboard (useful for sharing branch names)

cd ~/synapse-deployment-helpers
git checkout chatops
gccb
```

Paste action outputs:

```
chatops
```

#### `gccpb`

```sh
# Copies the current directory + current branch name in the clipboard (does NOT check if you're in a repository or not)

cd ~/synapse-deployment-helpers
git checkout chatops
gccpb
```

Paste action outputs:

```
synapse-deployment-helpers chatops
```
> Mainly useful to the `cimsg` command to generated full chatops messages

### Utils

All commands that do not fall in other categories.

#### `links`

```sh
# Tells how many npm or yarn links are active in your current repository (have to be a git repository, more useful if the project have some @synapse-medicine dependencies)

cd ~/path/to/repositories/Synapse
make install
yarn link @synapse-medicine/core
yarn link @synapse-medicine/boto

links
```

Output:

```
Linked packages:
- boto
- core
```

### `whatmake`

```sh
# Helps seeing what make commands are available in current project

cd ~/path/to/repositories/Synapse
whatmake build
whatmake GNRUNURGGUNUNEGUNUNRG
whatmake
```

Outputs:

```Makefile
# whatmake build
build:
  $(call create_app_version_helper, $(tag))
  docker build --build-arg NPM_TOKEN="$$NPM_TOKEN" -t ${registry_base}/${app_name}:${tag} -m 4G .

# whatmake GNRUNURGGUNUNEGUNUNRG
# Command "GNRUNURGGUNUNEGUNUNRG" does not exist in current project's Makefile.

# whatmake
version
all
res
build
config
clean
install
link
prepare
run
run-m1
android
ios
ios-test
ios-prod
android-test
android-prod
test
push
```

### Aliases

Aliases that might be helpful for various operations in various projects (mostly `make` commands).

> Regarding aliases, for OMZ users, those plugins are recommanded: git, kubectl

```sh
alias mc
# mc="make clean"
alias mi
# mi="make install"
alias mp
# mp="make prepare"
alias ml
# ml="make link"
alias mb
# mb="make build"
alias mbl
# mbl="make build-local"
alias mbt
# mbt="make build-test"
alias mr
# mr="make run"
alias mrl
# mrl="make run-local"
alias mrt
# mrt="make run-test"
alias mblmr
# mblmr="make build-local && make run"
alias mdv
# mdv="make dev"
alias mpb
# mpb="make publish"
alias mpbt
# mpbt="make publish-test"
alias mtr
# mtr="meteor"

# Clean install
cli
# Clean install + build in (usually) production mode
clib
# Clean install + build in (usually) dev mode
clibt
# Clean prepare (install + link)
clp
# Clean prepare + build in production mode
clpb
# Clean prepare + build in dev mode
clpbt

# Kubectl get pods, greps something
greppod

# Kubectl get deployments, jsonpath magic, and greps something (a docker image name)
# Useful to see exactly the running version of some project
grepimage
```

 ---
## Support

Check out [issues](https://gitlab.com/Bouga/synapse-deployment-helpers/-/issues).

## Project status

Project is alive and well.
