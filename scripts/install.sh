#!/bin/bash

# Copying files where it would be imported by .bashrc or something
mkdir ~/synapse-deployment-helpers
cp omz-plugin/*.zsh ~/synapse-deployment-helpers

# Script needs $HOME to exist, not very safe if absent
if [[ -z $HOME ]]; then
  echo "\"\$HOME\" is not set, you should set it first to ~."
  return 1
fi

# Asking what is the current "bashrc" (defaults to ~/.bashrc)
read -p "Where is your bash init file ? (defaults to $HOME): " init_folder
read -p "What is your bash init file ? (defaults to .bashrc): " bashrc

if [[ -z $init_folder ]]; then
  init_folder=$HOME
fi
if [[ -z $bashrc ]]; then
  bashrc=".bashrc"
fi

# Saving current bash init file just in case
cd $init_folder
cat $bashrc > "${bashrc}.pre-synapse"

# Adding sourcing for synapse-deployment-helpers in init file
# (All files on their own because bash sourcing is not friendly)
echo "" >> $bashrc
echo ". $HOME/synapse-deployment-helpers/synapse-deployment-helpers.plugin.zsh" >> $bashrc
echo ". $HOME/synapse-deployment-helpers/git.zsh" >> $bashrc
echo ". $HOME/synapse-deployment-helpers/utils.zsh" >> $bashrc
