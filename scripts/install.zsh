#!/bin/zsh

# Variable $ZSH should exist in "set" command output
ZSH="$(set | sed -n -E 's/^ZSH=(.*)$/\1/p')"

if [[ -z $ZSH ]] then
  echo "Var \$ZSH not found. Did you install Oh-my-zsh ? https://ohmyz.sh"
  exit 1
fi

# Creating synapse-deployment-helpers directory in custom plugins location
ZSH_CUSTOM="$ZSH/custom"
mkdir -p $ZSH_CUSTOM/plugins/synapse-deployment-helpers

# Copying the actual plugin script
cp omz-plugin/*.zsh $ZSH_CUSTOM/plugins/synapse-deployment-helpers

# Sourcing .zshrc
exec zsh
